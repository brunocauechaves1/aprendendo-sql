--Aula de SQL statement

/*Os comentários não são executados e são úteis para inserir descrições no seu código.

Nessa aula iremos aprender a instrução de SELECT que realiza a busca dos dados na tabela.
Para isso, precisaremos inicialmente criar e inserir dados nas tabelas.
Execute as instruções abaixo, mas não se preocupe nesse momento com elas. */

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number
);

insert into funcionario values (1, 'Ana', 22);
insert into funcionario values (2, 'Felipe', 55);
insert into funcionario values (3, 'Bianca', 2);
insert into funcionario values (4, 'Marcos', 10);
insert into funcionario values (5, 'Julio', 18);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--Utilizando o * na clausula SELECT irá retornar todas as colunas da tabela
--Na cláusula from é inserida a tabela de busca
select * from funcionario;

--Também é possivel filtrar o resultado pelo nome da coluna
select nome from funcionario;

--O comando de select não é case-sensitive
select Idade FROM Funcionario;

--A clausula Where irá limitar a busca
select * 
  from funcionario 
 where idade > 20;
 
--Pratique o comando de SELECT com a tabela abaixo

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 2);
insert into departamento values (2, 'dept2', 50);
insert into departamento values (3, 'dept3', 25);
insert into departamento values (4, 'dept4', 150);
insert into departamento values (5, 'dept5', 33);
insert into departamento values (6, 'dept5', 33);
insert into departamento values (7, 'dept5', 33);
insert into departamento values (8, 'dept5', null);

-- UTLIZANDO OO ALIAS
select num_dept numero_departamento from departamento;
select qtd_func as "quatidade de funcionario" from departamento;
select dept.qtd_func from departamento dept; -- usando alias pra tabela

--OPERADOR DE CONCATENAÇÃO E LITERAIS

select num_dept || qtd_func from departamento;
select num_dept || ' '|| qtd_func from departamento;
select num_dept || qtd_func numerodpt_quatidadefun from departamento;
select num_dept||'numero do departamento'||qtd_func||'quatidade de funcionario' departamento_funcionario from departamento;


-- QUOTE OPERATOR - sempre com aspas simples (aspas duplas nao funciona)
-- A SINTAXE INICIA COM q EM SEGUIDA ESCOLAHA UM CARACTER DE SUPORTE QUE DEVE ESTAR NO INICIO E NO FIM .
select q '(algumacois num_dept)' from departamento;
select q '2algumacoisa num_dept2' from departamento;


-- DISCTINCT - ~só mostar um dos duplicados
SELECT disctinct nom_dept, qtd_func as 'quatidade funcionario' from departamento;


--expressões aritméticas os calculos sao iguais da matematica
select qtd_func+100 from departamento;


--- valores nuloss sao diferentes de espaço em branco ou zero
select* from departamento;


/*operadores de comparação iniciais
	operador 	significado
		=			igual a
		>			maior que
		>=			maior que ou igual a 
		<			menor que
		<=			menor que ou igual a 
		<>			diferente de 
*/

select *from departamento
where nom_dept = 'dept5';


-- BETWENN OPERADOR DE COMPRAÇÃO
select nom_dept, qtd_func
from departamento
where nom_dept betwenn 'dept1' and 'dept5';
-- where qtd_func 2 and 150;




--- IN retornas todos os parametros iguals no solicitado
select num_dept, qtd_func
from departamento
where nom_dept in ('dept5','dept1','dept3');

-- LIKE OPERADOR DE COMPARAÇÃO DUAS CADEIAS DE CARACTER (STRING);
--'%5' pega tudo que vem antes do 5 'dep%' tudo que vem depois '%p%' pega o que vem antes e depois
select num_dept, qtd_func
from departamento
where nom_dept like ('dept5');

-- IS NULL VERIFICA SE UMA EXPESSÃO É NULA OU NAO

select num_dept, qtd_func
from departamento
where qtd_func is null;


/*OPERADORES LOGICOS
	NOT 	NEGAÇÃO
	AND 	AMBAS AS EXPRESSÕES DEVE SER ATENDIDA
	OR 		UPENAS UMA DAS EXPRESSÕES DEVE ATENDIDA
*/

select num_dept, qtd_func
from departamento
where nom_dept like 'dept5' and 33;


select num_dept, qtd_func
from departamento
where nom_dept like 'dept5' or 33 and num_dept = 6;

/* ORDER BY 
ASC - DEFAULT CRESCENT
DESC - DECRESCENT
NULL LAST  - OS VALORES NULOS POR ULTIMO
NULLS FIRST - VALORES NULUS POR PRIMEIRO
*/
select num_dept, qtd_func
from departamento
where nom_dept like 'dept5'
order by num_dept desc;


--Aula da clausula fetch

/*Nessa aula iremos aprender a utilizar a cláusula fetch
Novamente iremos utilizar a tabela funcionário de exemplo*/

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number
);

insert into funcionario values (1, 'Ana', 23);
insert into funcionario values (2, 'Ana', 25);
insert into funcionario values (3, 'Bianca', 46);
insert into funcionario values (4, 'Marcos', 33);
insert into funcionario values (5, null, null);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--Limita a quantidade de registros retornados pela consulta
select nome
  from funcionario
 fetch first 2 rows only; 
 
--first/next possui a mesma função
select nome
  from funcionario
 fetch first 2 rows only; --Mesmo retorno da consulta anterior
 
--row/rows - limita pelo número de linhas
select nome
  from funcionario
 fetch first 2 row only;--Mesmo retorno da consulta anterior
 
--percent rows only - limita pela porcentagem de registros
select nome
  from funcionario
 fetch first 20 percent rows only;
 
--with ties - se existir registros duplicados, pode retornar mais valores do que o especificado 
--Observação: quando utilizamos essa cláusula é recomendado o uso do group by na consulta
select nome
  from funcionario
 order by nome
 fetch first 20 percent rows with ties;
 
select nome
  from funcionario
 fetch first 20 percent rows with ties; --Não retorna o segundo nome 'Ana', mesmo com a clausula with ties devido a ausencia de group by

--OFFSET: inicia o retorno dos resultados a partir de uma determinada linha
select nome
  from funcionario
 order by nome
 offset 2 rows fetch first 2 rows only; 
 
--caso o offset for negativo, considera-se como 0
select nome
  from funcionario
 order by nome
 offset -5 rows fetch first 2 rows only; 
 
--Pratique o comando de SELECT com a tabela abaixo
drop table departamento;

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 56);
insert into departamento values (2, 'dept2', 100);
insert into departamento values (3, 'dept3', 600);
insert into departamento values (4, 'dept4', 10);
insert into departamento values (5, null, 34);










