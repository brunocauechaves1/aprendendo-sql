--Aula de Alias

/*Nessa aula iremos aprender a utilizar o alias que nos permite dar apelido para as colunas e tabelas da instrução SELECT. 
Novamente iremos utilizar a tabela funcionário de exemplo*/

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number
);

insert into funcionario values (1, 'Ana', 22);
insert into funcionario values (2, 'Felipe', 55);
insert into funcionario values (3, 'Bianca', 2);
insert into funcionario values (4, 'Marcos', 10);
insert into funcionario values (5, 'Julio', 18);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--O comando as é opcional, os dois selects abaixo retornam o mesmo resultado
select nome as nome_funcionario from funcionario;
select nome nome_funcionario from funcionario;

--Alias sem aspas: não é case-sensitive, deve ser uma palavra e não deve exceder 30 caracteres
select nome 2 from funcionario; --gera erro
select nome NOME_Funcionario from funcionario; 
select nome nome funcionario from funcionario; --gera erro

--Alias com aspas duplas: é case-sensitive. Pode usar maiúsculas e minúsculas e pode colocar espaço.
select nome "NOME_Funcionario" from funcionario; 
select nome as "nome funcionario" from funcionario; 

--Alias pode também ser usado como apelido de tabela
select f.nome FROM Funcionario f;
select f.nome FROM Funcionario f, funcionario c;
select f.nome FROM Funcionario f, funcionario f; --gera erro, não é possível colocar o mesmo alias em 2 tabelas da clausula from
select nome FROM Funcionario f, funcionario c; --gera erro, sql não saberá diferenciar de qual das tabelas deve retornar a coluna 'nome'

--alias em tabelas da clausula from podem ser utilizadas na clausula where
select nome
  from funcionario f
 where f.nome = 'Ana';
 
--alias em colunas da clausula select não podem ser utilizadas na clausula where
select nome nom
  from funcionario 
 where nom = 'Ana'; --gera erro
 
--Pratique o comando de SELECT com a tabela abaixo

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 2);
insert into departamento values (2, 'dept2', 50);
insert into departamento values (3, 'dept3', 25);
insert into departamento values (4, 'dept4', 150);
insert into departamento values (5, 'dept5', 33);

