--Aula de to_number e to_date

select to_date('01/10/2019', 'dd/mm/yyyy')
  from dual;
  
select to_date('01-10-2019', 'dd-mm-yyyy')
  from dual;
  
select to_date('01 november 2019', 'dd month yyyy')
  from dual;
  
select to_date('01/          10/2019', 'dd/mm/yyyy')
  from dual; --Oracle remove espaços em branco
  
select to_date('01/ 10/2019', 'fxdd/ mm/yyyy')
  from dual; --fx determina que seja exatamente o mesmo formato
  
select to_char(to_date('01/10/19', 'dd/mm/yy'), 'dd/mm/yyyy')
  from dual;
  
select to_char(to_date('01/10/89', 'dd/mm/yy'), 'dd/mm/yyyy')
  from dual;
  
select to_char(to_date('01/10/19', 'dd/mm/rr'), 'dd/mm/rrrr')
  from dual;
  
select to_char(to_date('01/10/89', 'dd/mm/rr'), 'dd/mm/rrrr')
  from dual;
  
select to_number('4567')
  from dual;
  
select to_number('$4567', '$9999')
  from dual;
  
select to_number('$2000.00', '$9999.99')
  from dual;