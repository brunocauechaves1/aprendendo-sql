--Aula de timestamp/timestamp with time zone

--Nessa aula iremos aprender sobre timestamp/timestamp with time zone

create table datas (
    data1 date,
    data2 timestamp,
    data3 timestamp with time zone
);

--sysdate retorna a data e hora corrente para o servidor
--current_timestamp retorna a data e hora corrente para o usuário da sessão (timestamp with time zone)
insert into datas values (sysdate, current_timestamp, current_timestamp);

select *
  from datas;
  
select to_char(data1, 'dd-mm-yyyy hh:mi:ss'), data2, data3
  from datas;