--Aula de funções de extração

--Nessa aula iremos aprender sobre funções de extração

select to_char(sysdate, 'yyyy') ano,
       to_char(sysdate, 'mm') mes,
       to_char(sysdate, 'dd') dia
  from dual;
  
select extract(year from sysdate) ano,
       extract(month from  sysdate) mes,
       extract(day from sysdate) dia
  from dual;
  
select extract(year from to_date('01/05/2018', 'dd/mm/yyyy')) ano,
       extract(month from  to_date('01/05/2018', 'dd/mm/yyyy')) mes,
       extract(day from to_date('01/05/2018', 'dd/mm/yyyy')) dia
  from dual;