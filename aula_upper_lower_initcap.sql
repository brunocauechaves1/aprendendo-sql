--Aula da funções de caracter(UPPER, LOWER, INITCAP)

/*Nessa aula iremos aprender a utilizar as funções de caracter
Novamente iremos utilizar a tabela funcionário de exemplo*/

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number
);

insert into funcionario values (1, 'Ana', 23);
insert into funcionario values (2, 'Ana', 25);
insert into funcionario values (3, 'Bianca', 46);
insert into funcionario values (4, 'Marcos', 33);
insert into funcionario values (5, null, null);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--Função UPPER: converte todos os caracteres para letras maiúsculas
select upper(nome)
  from funcionario;
--Muito utilizada para comparar duas string
select nome
  from funcionario
 where upper(nome)='ANA';
 
--Função LOWER: converte todos os caracteres para letras minúsculas
select lower(nome)
  from funcionario;
  
--Função INITCAP: Primeira letra de cada palavra como maiúscula e as outras letras em minúscula
select initcap(nome)
  from funcionario;
  
select initcap('ESSE CURSO É SOBRE ORACLE DATABASE SQL')
  from funcionario;

--Pratique o comando de SELECT com a tabela abaixo
drop table departamento;

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 56);
insert into departamento values (2, 'dept2', 100);
insert into departamento values (3, 'dept3', 600);
insert into departamento values (4, 'dept4', 10);
insert into departamento values (5, null, 34);

