--Aula de operadores de concatenação e literais 

/*Nessa aula utilizaremos operadores de concatenação e literais.
Para isso, execute o script abaixo*/

create table produto(
    id number primary key,
    nome varchar2(40),
    validade_meses number
);

insert into produto values (1, 'Bala', 180);
insert into produto values (2, 'Banana', 5);
insert into produto values (3, 'Chá', 50);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--O operador de concatenação || concatena duas colunas em uma só. 
--Observe que não é inserido espaço entre os valores e o nome da coluna ficará como 'nome||validade_meses'
select nome||validade_meses from produto;

--Para melhor apresentação da coluna, aconselhável utilizar um alias
select nome||validade_meses validade from produto;

--É possível inserir espaço em branco para separação dos campos na mesma coluna
select nome||' '||validade_meses validade from produto;

--Também podemos inserir uma cadeia de caracteres na concatenação (essas cadeias são chamadas de literais)
select nome||' vence em: '||validade_meses||' meses' validade from produto;

--Literais também podem ser retornados fixos no select 
select 'teste' literal
  from produto; 
  
--Pratique o comando de SELECT com a tabela abaixo

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 2);
insert into departamento values (2, 'dept2', 50);
insert into departamento values (3, 'dept3', 25);
insert into departamento values (4, 'dept4', 150);
insert into departamento values (5, 'dept5', 33);

