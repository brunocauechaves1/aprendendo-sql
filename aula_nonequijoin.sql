--Aula de nonequijoins

--Nessa aula iremos aprender sobre nonequijoins

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40),
    salario number
);

insert into funcionario values (1, 'Karol', 1800);
insert into funcionario values (2, 'Ana', 5000);
insert into funcionario values (3, 'Bianca', 3400);
insert into funcionario values (4, 'Marcos', 6000);
insert into funcionario values (5, 'João', 2300);
insert into funcionario values (6, 'Alan', 1600);

drop table grade;

create table grade(
    id number,
    menor_salario number,
    maior_salario number
);

insert into grade values (1, 1500, 2000);
insert into grade values (2, 2001, 3000);
insert into grade values (3, 3001, 4000);
insert into grade values (4, 4001, 5000);

select f.nome, f.salario, g.id, g.menor_salario, g.maior_salario
  from funcionario f, grade g
 where f.salario >= g.menor_salario
   and f.salario <= g.maior_salario; 
   
select f.nome, f.salario, g.id, g.menor_salario, g.maior_salario
  from funcionario f, grade g
 where f.salario between g.menor_salario and g.maior_salario; 
 
create table emprestimo(
    id number,
    valor number
);

insert into emprestimo values (1, 120000);
insert into emprestimo values (2, 50500);
insert into emprestimo values (3, 23000);
insert into emprestimo values (4, 1200);

create table juros_emprestimo(
    id number,
    valor_juros number,
    menor_emprestimo number,
    maior_emprestimo number
);

insert into juros_emprestimo values(1, 5, 0, 5000);
insert into juros_emprestimo values(2, 4, 5001, 10000);
insert into juros_emprestimo values(3, 3.5, 10001, 20000);
insert into juros_emprestimo values(4, 3, 200001, 40000);
insert into juros_emprestimo values(5, 2.5, 40001, 50000);
insert into juros_emprestimo values(6, 2, 50001, 100000);
insert into juros_emprestimo values(7, 1.5, 100001, 300000);