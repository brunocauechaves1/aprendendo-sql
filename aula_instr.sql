--Aula da funções de caracter (INSTR)

/*Nessa aula iremos aprender a utilizar as funções de caracter (INSTR)
Novamente iremos utilizar a tabela funcionário de exemplo*/

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40),
    sobrenome varchar2(40)
);

insert into funcionario values (1, 'Ana', 'Souza');
insert into funcionario values (2, 'Ana', 'Silva');
insert into funcionario values (3, 'Bianca', 'Oliveira');
insert into funcionario values (4, 'Marcos', 'Andrade');
insert into funcionario values (5, null, null);

/*Após a execução desses scripts podemos iniciar a utilização das funções*/

--INSTR: retorna a posição numérica de uma cadeia de caracteres em uma string
--Primeiro argumento é a string em que vou realizar a busca 
--Segundo argumento é o caracter ou a cadeia de caracteres que vou procurar
select instr(sobrenome, 'ra'), sobrenome
  from funcionario; --O retorno é a posição do primeiro caracter do 'ra'

--Mesmo que a palavra do primeiro argumento possua mais de uma ocorrencia do caracter que estou buscando, o valor retornado é da primeira ocorrencia.
select instr(sobrenome, 'd'), sobrenome
  from funcionario;   
  
--O terceiro argumento é a posição em que quero iniciar a busca
select instr(sobrenome, 'd', 4), sobrenome
  from funcionario;   
  
--O valor da posição pode ser negativo, nesse caso a contagem da posição inicial começará do final da string
select instr(sobrenome, 'd', -3), sobrenome
  from funcionario;   
  
--O quarto argumento indica a ocorrencia do caracter que queremos procurar a posição.
select instr(sobrenome, 'd', 1, 2), sobrenome
  from funcionario;   
  
--O INSTR é muito utilizado aninhado com a função SUBSTR
select substr(nome||' '||sobrenome, instr(nome||' '||sobrenome, ' ')+1), nome||' '||sobrenome nome_completo
  from funcionario;
  
--Pratique o comando de SELECT com a tabela abaixo
drop table departamento;

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    descricao varchar2(50)
);

insert into departamento values (1, 'dept1', 'informatica');
insert into departamento values (2, 'dept2', 'segurança do trabalho');
insert into departamento values (3, 'dept3', 'rh');
insert into departamento values (4, 'dept4', 'contabilidade');

