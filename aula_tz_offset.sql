--Aula de tz_offset

--Nessa aula iremos aprender sobre tz_offset

select tz_offset('Asia/Dubai'), tz_offset('America/Chicago')
  from dual;
  
--from_tz
/*Função que retorna conversão de timestamp para 
timestamp with time zone de acordo com a timezone informada */
select from_tz(timestamp '2000-03-6 08:00:00', 'America/Chicago')
  from dual;