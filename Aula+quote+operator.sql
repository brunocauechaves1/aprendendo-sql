--Aula de quote operator 

/*Nessa aula utilizaremos quote operator.
Para isso, execute o script abaixo*/

create table produto(
    id number primary key,
    nome varchar2(40),
    validade_meses number
);

insert into produto values (1, 'Bala', 180);
insert into produto values (2, 'Banana', 5);
insert into produto values (3, 'Chá', 50);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--quote operator nos permite inserir apostrofo em strings literais sem que ocorram erros internos no oracle
select 'It's important' from produto; --gera erro

--Para melhor apresentação da coluna, aconselhável utilizar um alias
select q'[It's important]' quote_operator from produto; 
select q'aIt's importanta' quote_operator from produto; 
select q'2It's important2' quote_operator from produto; 
select q'%It's important%' quote_operator from produto; 
select q"%It's important%" quote_operator from produto; --gera erro

--Pratique o comando de SELECT com a tabela abaixo

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 2);
insert into departamento values (2, 'dept2', 50);
insert into departamento values (3, 'dept3', 25);
insert into departamento values (4, 'dept4', 150);
insert into departamento values (5, 'dept5', 33);

select q'AIt1's importantA' quote_operator from departamento;
select q'ZIt s importantZ' quote_operator from departamento;
select q'HnovinhaH' quote_operator from departamento;
select qLmelhorL quote_operator from departamento; -- erorou
select q'2o mlhor do brasil2' quote_operator from departamento;