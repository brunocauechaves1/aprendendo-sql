--Aula de privilégios 

--Nessa aula iremos aprender sobre privilégios 

--verificar os usuários disponíveis
select *
  from all_users;
  
--para verificar os privilégios
select *
  from system_privilege_map;
  
--para criar um novo usuário
create user teste identified by teste;

--privilégios de sistema
grant create session to teste;

--também podemos fazer: grant create session, create table to teste;
grant create table to teste;

grant unlimited tablespace to teste with grant option;

grant create sequence to teste;

grant create view to teste;

grant create synonym to teste;

create table cidade(
    id number primary key,
    nome varchar2(50)
);

--privilégios de objetos
grant select on cidade to teste;

grant delete on cidade to teste with grant option;

grant update (nome) on cidade to teste;

grant all on cidade to teste;

grant select, insert to teste;

grant select on cidade to public;

revoke delete on cidade from teste;
