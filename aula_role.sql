--Aula de role 

--Nessa aula iremos aprender sobre role 

create role gerente;

grant create table, create view, create sequence
to gerente;

select *
  from role_sys_privs
 where role = 'gerente';
 
create user teste identified by teste;

grant create session to teste;

grant unlimited tablespace to teste;

grant gerente to teste;

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40) not null,
    salario number,
    genero char,
    created_date date default sysdate, 
    created_by varchar2(100) default user
);

create role IUP_funcionario;

grant insert, update, delete on funcionario to IUP_funcionario;

select *
  from role_tab_privs
 where role = upper('IUP_funcionario');
  
