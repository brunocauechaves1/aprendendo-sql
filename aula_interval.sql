--Aula de interval (year to month/day to second)

--Nessa aula iremos aprender sobre (year to month/day to second)

--ano pode ser uma valor positivo ou negativo
--mês 00-11
--dia pode ser um valor positivo ou negativo
--hora 00-23
--minuto 00-59
--segundo 00 to 59.9

select interval '55-11' year to month
  from dual;
  
select interval '1-4' year to month
  from dual;
  
select interval '555-11' year to month
  from dual; --se o ano for maior que 2 digitos, é necessário especificar o tamanho
  
select interval '555-11' year(3) to month
  from dual;
  
select interval '1000-11' year(4) to month
  from dual;
  
select interval '350' month
  from dual;
  
select interval '10' year
  from dual;
  
select interval '33 20:20:20' day to second
  from dual;
  
select interval '333 20:20:20' day(3) to second
  from dual;
  
select interval '500' hour
  from dual;