--Aula de cross join

--Nessa aula iremos aprender sobre cross join

drop table funcionario;

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number,
    dept number
);

insert into funcionario values (1, 'Karol', 23, 1);
insert into funcionario values (2, 'Ana', 18, 1);
insert into funcionario values (3, 'Bianca', 16, 1);
insert into funcionario values (4, 'Marcos', 10, 2);
insert into funcionario values (5, 'João', null, 2);
insert into funcionario values (6, 'Alan', 10, 2);

drop table departamento;

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    num_func number
);

insert into departamento values (1, 'dept1', 12);
insert into departamento values (2, 'dept2', 455);
insert into departamento values (3, 'dept3', 67);
insert into departamento values (4, 'dept4', null);

--Sintaxe de produto cartesiano (old join)
select f.nome, d.nom_dept
  from funcionario f, departamento d
 order by nome; 
 
--Sintaxe com cross join
select f.nome, d.nom_dept
  from funcionario f cross join departamento d
 order by nome;
 
drop table pedido;

create table pedido(
    id number,
    cliente varchar2(50)
);

insert into pedido values (1, 'Ana');
insert into pedido values (2, 'José');
insert into pedido values (3, 'Bianca');

drop table pedido_detalhe;

create table pedido_detalhe(
    id_det number,
    id_ped number,
    item varchar2(50)
);

insert into pedido_detalhe values (1, 1, 'Banana');
insert into pedido_detalhe values (2, 1, 'Uva');
insert into pedido_detalhe values (3, 2, 'Shampoo');
insert into pedido_detalhe values (4, 2, 'Sabonete');
insert into pedido_detalhe values (5, 3, 'Blusa');