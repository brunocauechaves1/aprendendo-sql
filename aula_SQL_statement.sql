--Aula de SQL statement

/*Os comentários não são executados e são úteis para inserir descrições no seu código.

Nessa aula iremos aprender a instrução de SELECT que realiza a busca dos dados na tabela.
Para isso, precisaremos inicialmente criar e inserir dados nas tabelas.
Execute as instruções abaixo, mas não se preocupe nesse momento com elas. */

create table funcionario(
    id number primary key,
    nome varchar2(40),
    idade number
);

insert into funcionario values (1, 'Ana', 22);
insert into funcionario values (2, 'Felipe', 55);
insert into funcionario values (3, 'Bianca', 2);
insert into funcionario values (4, 'Marcos', 10);
insert into funcionario values (5, 'Julio', 18);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/

--Utilizando o * na clausula SELECT irá retornar todas as colunas da tabela
--Na cláusula from é inserida a tabela de busca
select * from funcionario;

--Também é possivel filtrar o resultado pelo nome da coluna
select nome from funcionario;

--O comando de select não é case-sensitive
select Idade FROM Funcionario;

--A clausula Where irá limitar a busca
select * 
  from funcionario 
 where idade > 20;
 
--Pratique o comando de SELECT com a tabela abaixo

create table departamento(
    num_dept number primary key,
    nom_dept varchar2(50),
    qtd_func number
);

insert into departamento values (1, 'dept1', 2);
insert into departamento values (2, 'dept2', 50);
insert into departamento values (3, 'dept3', 25);
insert into departamento values (4, 'dept4', 150);
insert into departamento values (5, 'dept5', 33);

