--Aula de Operadores de comparação

/*Nessa aula trabalharemos com operadores de comparação.
Para isso, execute o script abaixo*/

drop table pessoa;

create table pessoa(
    id number primary key,
    nome varchar2(40),
    salario number
);

insert into pessoa values (1, 'Maria', 100);
insert into pessoa values (2, 'João', 200);
insert into pessoa values (3, 'Joana', 50);
insert into pessoa values (4, 'Katia', null);

/*Após a execução desses scripts podemos iniciar as instruções de busca*/
--Operador de igualdade (=)
select *
  from pessoa
 where salario = 100;
 
select *
  from pessoa
 where nome = 'Maria';
 
select *
  from pessoa
 where nome = 'maria'; --Não retornará valor, pois os operadores de comparação são case-sensitive
  
--Operador maior que e maior igual que (>, >=)
select *
  from pessoa
 where salario > 100;
 
select *
  from pessoa
 where salario >= 100;
 
--Operador menor que e menor igual que (<, <=)
select *
  from pessoa
 where salario < 100;
 
select *
  from pessoa
 where salario <= 100;
 
--Operador diferente de (<>)
select *
  from pessoa
 where salario <> 100;
 
 
--Expressões de comparação também funcionam para strings (Oracle irá compar o código ASCII de cada letra)
select *
  from pessoa
 where nome <= 'Katia';
 
 'Katia' < 'Kevin' --K é igual
                    --a < e sim

drop table roupa;

create table roupa(
    id number primary key,
    tipo varchar2(50),
    preço varchar2(50)
);

insert into roupa values (1, 'blusa', 30);
insert into roupa values (2, 'calça', null);
insert into roupa values (3, 'jaqueta', 70);

select tipo, preço from roupa where tipo < 'calça';

select tipo from roupa where tipo >= 'calça';
